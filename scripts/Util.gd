extends Node
class_name Util

static func array2d(width,height):
	var matrix = []
	for x in range(width):
		matrix.append([])
		for y in range(height):
			matrix[x].append(0)
	return matrix
