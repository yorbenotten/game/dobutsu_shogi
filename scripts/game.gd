extends Node2D

enum PLAYER {ONE, TWO}

onready var _cell = preload("res://scenes/cell.tscn")
onready var _lion = preload("res://scenes/game_pieces/lion.tscn")
onready var _giraffe = preload("res://scenes/game_pieces/giraffe.tscn")
onready var _elephant = preload("res://scenes/game_pieces/elephant.tscn")
onready var _chicken = preload("res://scenes/game_pieces/chicken.tscn")

onready var player_one
onready var player_two

var board
onready var current_player = PLAYER.ONE
var selected_piece = null
var valid_moves = []

func _ready():
	new_game(Vector2(3,4))

func new_game(size):
	board = get_node("/root/Dobutsu/CenterContainer/Board")
	player_one = get_node("/root/Dobutsu/PlayerOne")
	player_two = get_node("/root/Dobutsu/PlayerTwo")
	board.init(size)
	board.draw_cells(size, _cell)
	place_pieces()

func place_pieces():
	###### PLAYER ONE
	board.place_game_piece(1,3,_lion.instance(),  PLAYER.ONE)
	board.place_game_piece(0,3,_elephant.instance(),  PLAYER.ONE)
	board.place_game_piece(2,3,_giraffe.instance(),  PLAYER.ONE)

	var chicken = _chicken.instance()
	chicken.init(PLAYER.ONE)
	board.place_game_piece(1,2,chicken,  PLAYER.ONE)

	###### PLAYER TWO
	board.place_game_piece(1,0,_lion.instance(),  PLAYER.TWO)
	board.place_game_piece(2,0,_elephant.instance(),  PLAYER.TWO)
	board.place_game_piece(0,0,_giraffe.instance(),  PLAYER.TWO)

	var chicken2 = _chicken.instance()
	chicken2.init(PLAYER.TWO)
	board.place_game_piece(1,1,chicken2, PLAYER.TWO)

func show_move(pos):
	board.show_move(pos)

func show_all_indicators():
	board.show_all_indicators()

func set_place_captured(piece):
	selected_piece = piece
	calculate_valid_moves_for_captured_piece()
	show_all_indicators()

func calculate_valid_moves_for_captured_piece():
	var free_cells = board.free_cells()
	for cell in free_cells:
		valid_moves.append(cell.grid_position)

func flip_player(player):
	if(player == PLAYER.ONE):
		return PLAYER.TWO
	return PLAYER.ONE

func hide_indicators():
	board.hide_indicators()

func change_turn():
	selected_piece = null
	valid_moves = []
	if current_player == PLAYER.ONE:
		current_player = PLAYER.TWO
		return
	current_player = PLAYER.ONE


func is_move_valid(move):
	return valid_moves.has(move)

func make_move(piece, cell):
	board.hide_indicators()

	if(is_move_valid(cell.grid_position)):

		if(piece.captured):
			cell.add_game_piece(piece)
			return true

		var target_piece = cell.get_game_piece()
		if(target_piece):
			if(target_piece.player != current_player):
				cell.remove_game_piece()
				target_piece.capture()

		var old_cell = board.get_cell(piece.current_position)
		if(old_cell):
			old_cell.remove_game_piece()
			cell.add_game_piece(piece)
		return true

	selected_piece = null
	valid_moves = []
	return false

func cell_pressed(cell, game_piece):
	if(selected_piece):
		var move_made = make_move(selected_piece, cell)
		if(move_made):
			change_turn()
	elif(game_piece):
		valid_moves = []
		on_game_piece_clicked(game_piece)

func show_valid_moves():
	for move in valid_moves:
		show_move(move)

func on_game_piece_clicked(piece):
	valid_moves = []
	board.hide_indicators()
	if(piece.player != current_player):
		return
	selected_piece = piece
	selected_piece.z_index = 2

	var moves = piece.get_moves()
	for move in moves:
		var abs_board_pos = piece.current_position + move
		if(validate_move(abs_board_pos)):
			valid_moves.append(abs_board_pos)
	show_valid_moves()

func get_current_player():
	return current_player

func validate_move(move):
	if(board.check_bounds(move)):
		var game_piece = board.get_cell(move).get_game_piece()
		if(game_piece):
			if(game_piece.player == current_player):
				return false
		return true
	return false
