extends Node

class_name Math

static func calculate_cell_size(rectangle, nonogram):
	var x = int(rectangle.x / nonogram.x )
	var y = int(rectangle.y / nonogram.y )
	var smallest = min(x,y)
	var xoffset = fmod(rectangle.x, nonogram.x) / 2
	var yoffset = fmod(rectangle.y, nonogram.y) / 2
	
	return [Vector2(smallest,smallest), Vector2(int(xoffset), int(yoffset))]
