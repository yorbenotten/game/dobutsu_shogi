extends Area2D

enum PIECE_STATE {ACTIVE}
var state = PIECE_STATE.ACTIVE
var grid_position = Vector2()
onready var indicator = $PlacementIndicator

onready var click_reset = true

func _ready():
	set_process_input(true)
	$Sprite.play("unsolved")

func show_placement_indicator(active=true):
	if(active):
		indicator.visible = true
		return
	indicator.visible = false

func add_game_piece(piece):
	piece.visible = true
	if(piece.captured):
		SignalManager.emit_signal("captured_piece_placed", piece)
	piece.captured = false
	add_child(piece)
	piece.current_position = grid_position
	piece.set_position(Vector2(10,10))

func remove_game_piece():
	var piece = get_node("GamePiece")
	remove_child(piece)

func get_game_piece():
	return $GamePiece


func _on_Tile_input_event(viewport:Node, event:InputEvent, shape_idx:int):
	if (event.is_action_pressed("left_mouse_button")):
		Game.cell_pressed(self, $GamePiece)