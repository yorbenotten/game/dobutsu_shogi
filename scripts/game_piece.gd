extends Area2D

var player
var grid_position = Vector2()
var _type
var _moves = []
var current_position = Vector2()
var captured = false
var flipped = false


func _init(type = "", player = Game.PLAYER.ONE).():
	_type = type

func _ready():
	$Sprite.play("unsolved")

func get_type():
	return _type	

func get_moves():
	return _moves

func reset_position():
	z_index = 1
	position = Vector2(10,10)

func capture():
	SignalManager.emit_signal("game_piece_captured", self)
	player = Game.flip_player(player)
	flipped = !flipped
	$Sprite.flip_v = flipped
	captured = true
	visible = false

func get_player():
	return player