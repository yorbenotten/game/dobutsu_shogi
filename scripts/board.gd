extends Panel

var board
var	board_size
export var CELL_SIZE = 128

func _ready():
	.set_position(Vector2(0,0))

func show_all_indicators():
	var cells = free_cells()

	for cell in cells:
		cell.show_placement_indicator(true)

func free_cells():
	var cells = []
	for x in board:
		for y in x:
			var piece = y.get_game_piece()
			if(!piece):
				cells.append(y)
	return cells

func init(size):
	board_size = size
	board = Util.array2d(size.x,size.y)

func add_cell(x,y, cell):
	board[x][y] = cell

func get_cell(pos):
	return board[pos.x][pos.y]

func hide_indicators():
	for x in board:
		for y in x:
			y.show_placement_indicator(false)

func place_game_piece(x, y, piece, player):
	# Flip the sprite if second player
	if(player == Game.PLAYER.TWO):
		var s = piece.get_node("Sprite")
		s.flip_v = true
		piece.flipped = true

	piece.player = player
	piece.current_position = Vector2(x,y)
	var c = board[x][y]
	c.add_game_piece(piece)

func place_game_piece_on_cell(game_piece, cell):
	pass

func show_move(pos):
	var c = board[pos.x][pos.y]
	c.show_placement_indicator(true)

func get_cell_at_pos(pos):
	var a = pos - self.rect_position
	var b =  a / CELL_SIZE
	var c = Vector2(int(b.x), int(b.y))
	if(check_bounds(c)):
		return board[c.x][c.y]
	return null

func get_piece_at(pos):
	var cell = get_cell_at_pos(pos)
	if(cell):
		return cell.get_game_piece()
	return null

func check_bounds(pos):
	return (pos.x < board_size.x and pos.y < board_size.y and pos.x >= 0 and pos.y >= 0)


func print_board():
	for x in board:
		for y in x:
			print(y, " ", y.grid_position)

func draw_cells(size, cell):
	var dobutsu_pos  = Math.calculate_cell_size(self.rect_size, size)
	var cell_size = dobutsu_pos[0].x
	var offset = dobutsu_pos[1]	

	var rect = .get_rect().position

	var xcount = 0
	for x in range(size.x):
		var ycount = 0
		for y in range(size.y):
			var t = cell.instance()
			t.grid_position = Vector2(x,y)
			var s = t.get_node("Sprite")
			t.scale = Vector2(cell_size/CELL_SIZE,cell_size/CELL_SIZE)
			t.position = Vector2(rect.x  -xcount + (cell_size * x) + offset.x, rect.y + (cell_size * y) + offset.y -ycount)
			add_cell(x,y,t)
			add_child(t)
			ycount = ycount +1
		xcount = xcount +1

func _on_Board_mouse_exited():
	Game.reset_selected_piece()
