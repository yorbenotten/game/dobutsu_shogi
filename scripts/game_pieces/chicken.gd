extends "res://scripts/game_piece.gd"


func _init(type = "Chicken").():
	_type = type

func init(player):
	if(player == Game.PLAYER.ONE):
		_moves = [Vector2(0,-1)]
	else:
		_moves = [Vector2(0,1)]

func get_moves():
	if(player == Game.PLAYER.ONE):
		 return [Vector2(0,-1)]
	else:
		 return [Vector2(0,1)]