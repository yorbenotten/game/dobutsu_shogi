extends "res://scripts/game_piece.gd"


func _init(type = "Elephant").():
	_type = type
	_moves = [Vector2(-1,-1), Vector2(1,1), Vector2(1,-1), Vector2(-1, 1)]