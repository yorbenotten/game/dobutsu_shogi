extends "res://scripts/game_piece.gd"


func _init(type = "Lion").():
	_type = type
	_moves = [Vector2(-1,-1), Vector2(0,-1), Vector2(1,-1), Vector2(-1,0), Vector2(1,0), Vector2(-1,1), Vector2(0,1), Vector2(1,1)]
