extends "res://scripts/game_piece.gd"


func _init(type = "Giraffe").():
	_type = type
	_moves = [Vector2(0,1), Vector2(0,-1), Vector2(-1,0), Vector2(1,0)]