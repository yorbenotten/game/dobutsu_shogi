extends Position2D


var captured = { }

enum Player {ONE, TWO}
export (Player) var player

onready var chickens = $Captured/Chickens
onready var giraffes = $Captured/Giraffes
onready var elephants = $Captured/Elephants

var selected_piece = null


func _ready():
	chickens.visible = false
	giraffes.visible = false
	elephants.visible = false
	captured = {
		"Giraffe" : [],
		"Chicken" : [],
		"Elephant" : []
	}
	SignalManager.connect("game_piece_captured", self, "_on_game_piece_captured")
	SignalManager.connect("captured_piece_placed", self, "_on_captured_piece_placed")

func _on_captured_piece_placed(piece):
	var has_captured = captured[piece.get_type()].has(piece)
	if(has_captured):
		captured[piece.get_type()].erase(piece)
		update_labels()


#Gets called from game_piece
func _on_game_piece_captured(piece):
	if(piece.get_player() != player):
		add_captured(piece)

func add_captured(piece):
	captured[piece.get_type()].append(piece)
	update_labels()


func update_labels():
	for t in captured:
		var piece_count = captured[t].size()
		match t:
			"Chicken":
				update_label(chickens, piece_count)
			"Giraffe":
				update_label(giraffes, piece_count)
			"Elephant":
				update_label(elephants, piece_count)
			_:
				print_debug("piece type does not exist")

func update_label(captured_piece, count):
	captured_piece.visible = (count > 0)
	captured_piece.get_node("Label").set_text(str(count))

func _on_Chickens_gui_input(event:InputEvent):
	handle_left_mouse(event, "Chicken")

func _on_Giraffes_gui_input(event:InputEvent):
	handle_left_mouse(event, "Giraffe")

func _on_Elephants_gui_input(event:InputEvent):
	handle_left_mouse(event, "Elephant")

func handle_left_mouse(event, type):
	# If it's not the player's turn, nothing should happen when the player clicks
	if(Game.current_player != player):
		return

	if(event.is_action_pressed("left_mouse_button")):
		if captured[type].size() > 0:
			var piece = captured[type][0] 
			# Todo: replace this with signals 
			Game.set_place_captured(piece)
